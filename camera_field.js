// $Id: 

function camera_field_Take(mid,murl,mturl,preview) {
	//alert(mid+' '+murl+' '+mturl+' '+preview);
	$('#edit-mid').val(mid);
	$('#edit-filepath-thumbnail').val(mturl);
	if (preview == 'true') {
	  $('#camera_field_preview').show();
	  $('#camera_field_preview_img').attr('src',mturl);
	  location.href = '#mugshot_preview';
	} else {
	  $('#camera_field_preview').hide();
	}
	$('#camera_field_help').hide();
	$('#edit-submit').removeAttr("disabled");
	$('#edit-preview').removeAttr("disabled");
}

function camera_field_Test(url) {
  $.post(
    url,
    {},
    function(data) {
      var query_params = data.split('&');
      var array_query_params = new Array();
      for (var i=0;i<query_params.length;i++) {
        var tmp_query_params = query_params[i].split('=');
        array_query_params[tmp_query_params[0]] = unescape(tmp_query_params[1]);
      }
      if (array_query_params['result'] == 'OK') {
    	  camera_field_Take(
          array_query_params['mid'],
          array_query_params['murl'],
          array_query_params['mturl'],
          array_query_params['preview']
        );
      } else {
        alert(data);
      }
    }
  );
}

$(document).ready(function() {
	if ($('#edit-mid').val() == '') {
	  $('#camera_field_preview').hide();
	  $('#camera_field_help').show();
	  $('#edit-submit').attr("disabled", "disabled");
	  $('#edit-preview').attr("disabled", "disabled");
	} else {
	  $('#camera_field_preview').show();
	  $('#camera_field_help').hide();
	  $('#camera_field_preview_img').attr('src',$('#edit-filepath-thumbnail').val());
	}
});

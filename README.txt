$Id: 

CAMERA FIELD.MODULE
--------------------------------------------------------------------------------
This module ads new CCK field for capturing images from webcam. Captured images are displayed as 
thumbnails linked to original image. This module is based on Mugshot module.


MAINTAINERS
--------------------------------------------------------------------------------
Tamer Zoubi - <tamerzg@gmail.com>

SPONSORED BY

--------------------------------------------------------------------------------
Totally Techy